# Git gud

Time to git gud, son.

## Exercício prático

### 1. Init, add e commit
Usando a linha de comandos:
1. Cria um repositório novo num diretório vazio via `git init`. Corre `git status`.
2. Cria um ficheiro hello.txt e adiciona-lhe texto. Corre `git status`.
3. Adiciona o ficheiro à staging area via `git add hello.txt`. Corre `git status`.
4. Dá commit ao ficheiro via `git commit`. Corre `git status`.
5. Repete o exercício usando apenas a interface do VSCode (o `git status` aparece na tab de git, cria outro repositório noutra pasta)

#### Opcionais:
5. Adiciona mais ficheiros, dá-lhes commit.
6. Experimenta o comando `git add --patch`.

### 2. Branching e merging
Usando a linha de comandos/o VSCode/ambos:
1. Corre `git config --global alias.adog "log --all --decorate --oneline --graph"`.
2. Cria um branch via `git branch novo`. Corre `git adog`. Onde está a HEAD?
3. Corre `git checkout novo`. Corre `git adog` de novo. Corre `git status`. Onde está a HEAD?
4. Altera o ficheiro hello.txt: tira todo o texto existente e mete um texto diferente. Faz commit. Corre `git status` e `git adog`.
5. Muda para o branch principal (main ou master conforme a versão) via `git checkout ...`. Corre `git status` e `git adog`.
6. Repete o passo 4, com outro texto diferente (diferente do inserido no exercício 1 e diferente do passo 4). Não te esqueças de fazer commit e correr `git status` e `git adog`.
7. Faz `git merge novo`. O que diz o comando? Corre `git status` e abre o ficheiro hello.txt num editor de texto básico (Bloco de Notas/Notepad++). O que... notas?
8. Resolve o conflito: edita o ficheiro para ficar como entenderes, faz `git add` para sinalizar que está resolvido. Corre `git status`. Faz commit. Corre `git adog`.
9. Adiciona e faz commit a dois novos ficheiros. Assegura-te que estás no branch main/master. Corre `git adog`.
10. Vai para o branch novo. Corre `git adog`. Onde está a HEAD?
11. Faz `git merge main`. O que diz o comando? Nota que diz fast-forward.

### 3. Desenvolvimento com um repositório remoto
Usando a(s) interface(s) que preferires:
1. Cria um repositório com README no GitLab.
2. Clona o repositório para o teu PC. (Pode dar problemas de credenciais, tenta usar o teu google-fu para contornares o problema, se demorar mais que 5 min, apita)
3. Cria dois ficheiros e faz dois commits, um ficheiro para cada. Corre `git adog`. Onde está a HEAD? Onde está a origin?
4. Corre `git branch -vv`. O que diz sobre o main e sobre a origin?
5. Corre `git push`. Corre `git adog`.
6. No GitLab faz um commit através da interface online.
7. Corre `git fetch`. Corre `git branch -vv`. O que diz sobre o main e sobre a origin?
8. Faz um commit local que crie conflito com o commit que fizeste no GitLab.
9. Faz `git pull`. Corre `git status` e `git adog`. O que diz?
10. Resolve o conflito.
11. Corre `git push`. Corre `git adog`.

### ∞. Exercício perpétuo
1. Usar o git sempre que código estiver envolvido.
2. Corre `git status` e `git adog`.
3. ????
4. Git gud.
